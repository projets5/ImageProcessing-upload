import Queue from "bull"

// Singleton
let imageQueueSingleton = null;
export default class ImageQueue {
    constructor() {
        if(!imageQueueSingleton) {
            imageQueueSingleton = this;

            // Start the bull queue
            this._q = new Queue("ImageQueue", __REDIS_URL__);
        }

        // return singleton
        return imageQueueSingleton;
    }

    push(data) {

        this._q.add(data, {
            removeOnComplete: true,
            ...data.options
        });

        return this;
    }
}

// Keep in sync with ImageProcessing-upload
export const JobTypes = {
    VehiclesDetection: "VehiclesDetection",
}
