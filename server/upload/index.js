"use strict"

import { Router } from "express"
import promiseSettle from "promise-settle"
import formidable from "express-formidable"
import axios from "axios";
import pathLib from "path"
import mv from "mv"
import del from "del"

import * as uploadUtils from "./utils"
import startJobs from "./jobs"

const routes = Router();

routes.get("/ping", (req, res) => {
    res.json({time: Date.now()})
})



routes.get("/image/:cameraId", (req, res) => {

    res.sendFile(pathLib.join(__CAMERAS_DIR__, req.params.cameraId, "0.jpg"), {}, (err) => {
    // res.sendFile(pathLib.join(__CAMERAS_DIR__, "0", "0.jpg"), {}, (err) => {

        if(err) {
            res.sendFile(pathLib.join(__CAMERA_PLACEHOLDER__));
        }

    });
})


// Used to test a chunked packet with the mbed
routes.post("/chunked", (req, res) => {

    var data = "";
    req.on("data", (chunk) => {

        data += chunk;
        // console.log(String(chunk))
    })
    req.on("end", () => {
        console.log(data)
        console.log("==== END ====".green)
        res.json({time: Date.now()}).end();
    })

})

// uploadUtils.cleanMediaFolder()(()=>{}, ()=>{}, ()=>{}) // Careful, it's gonna delete all media at start/restart


// For data set generation
routes.post("/datasets",
    formidable({
        encoding: "utf-8",
        uploadDir: __MEDIA_TMP_DIR__,
        multiples: true,
    }),
    function(req, res) {
        let files = req.files.media || [];

        if(!Array.isArray(files)) {
            // Formidable doesn't use an array if there is only 1 file
            files = [files];
        }

        // Parse the files (1 file per camera)
        files = uploadUtils.parseDatasetFiles(files);

        Promise.resolve()
        .then(() => {
            // Job done
            res.json({
                time: Date.now(),
            })
            .end();
        })
        .then(() => {
            // Move files into their final location
            return promiseSettle(files.map((file) => {
                return new Promise((resolve, reject) => {
                    // Move the file
                    const src = file.path;
                    const dest = pathLib.join(__DATASETS_DIR__, file.cameraId, file.name);

                    // Move the image in its proper folder (create folder if needed)
                    mv(src, dest, { mkdirp: true }, (err) => {
                        if(err){
                            reject(file)
                        } else {
                            resolve({
                                ...file,
                                path: dest, // update path
                            })
                        }
                    })
                })
            }))
        })
        .then((promises) => {
            // Filter errs
            return promises.map((p, index) => {
                if(p.isFulfilled()) {
                    return p.value();
                } else {
                    // TODO remove the tmp file
                    console.error(p.reason());

                    return { // TODO test
                        isErr: true,
                        uuid: "7bf72743cc614f538a40cfba9fb3b30e", // Internal error for file
                        args: uploadUtils.parseFileForErr(files[index]),
                    }
                }
            })
        })
        .catch((err) => {
            console.error(err);
        })
    }
)

routes.post("/",
    // uploadUtils.cleanMediaFolder(), // <------- use for dev only
    formidable({
        encoding: "utf-8",
        uploadDir: __MEDIA_TMP_DIR__,
        multiples: true,
    }),
    function(req, res) {
        let files = req.files.media || [];

        if(!Array.isArray(files)) {
            // Formidable doesn't use an array if there is only 1 file
            files = [files];
        }

        // Parse the files (1 file per camera)
        files = uploadUtils.parseFiles(files);

        // Make sure the camera exist in the database
        // let url = uploadUtils.buildAPIUrl("camera", cameraId);

        // Make the first call
        // axios.get(url) // TODO database call (Make sure each camera exist in the database ?)
        Promise.resolve()
        .then(() => {
            // Job done
            res.json({
                time: Date.now(),
            })
            .end();
        })
        .then(() => {
            // Move files into their final location
            return promiseSettle(files.map((file) => {
                return new Promise((resolve, reject) => {
                    // Move the file
                    const src = file.path;
                    const dest = pathLib.join(__CAMERAS_DIR__, file.cameraId, file.name);

                    // Move the image in its proper folder (create folder if needed)
                    mv(src, dest, { mkdirp: true }, (err) => {
                        if(err){
                            reject(file)
                        } else {
                            resolve({
                                ...file,
                                path: dest, // update path
                            })
                        }
                    })
                })
            }))
        })
        .then((promises) => {
            // Filter errs
            return promises.map((p, index) => {
                if(p.isFulfilled()) {
                    return p.value();
                } else {
                    // TODO remove the tmp file
                    console.error(p.reason());

                    return { // TODO test
                        isErr: true,
                        uuid: "7bf72743cc614f538a40cfba9fb3b30e", // Internal error for file
                        args: uploadUtils.parseFileForErr(files[index]),
                    }
                }
            })
        })
        .then((images) => {

            // Filter valid images
            const validImages = images.filter((image) => !image.isErr);

            // Add new jobs
            startJobs(validImages)// Return a promiseSettle
            .then((promises) => {
                promises.forEach((p) => {
                    if(p.isFulfilled()) {
                        // const job = p.value();

                        // job.on("completed", () => {
                        //     // TODO job progress ?
                        //     console.log(job.data.type)
                        // })

                        // job.on("failed", (job, err) => {
                        //     // TODO log this err ?
                        //     console.log(err);
                        // })
                    } else {
                        console.log(p.reason());
                    }
                })
            })
            .catch((err) => {
                // TODO ?
                console.error(err)
            })
        })
        .catch((err) => {
            console.error(err);

            // res.status(500)
            // .json({ // TODO test
            //     isErr: true,
            //     uuid: "7bf72743cc614f538a40cfba9fb3b30e", // Internal error for file
            //     args: uploadUtils.parseFileForErr(files[index]),
            // })
        })
    }
)

export default routes;
