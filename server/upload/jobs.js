/*
    Jobs to execute are added to the queue here

    TODO:
        priority doesn't work for us:
            https://github.com/OptimalBits/bull/issues/733
*/

import promiseSettle from "promise-settle"

import ImageQueue, {
    JobTypes as ImageQueueJobTypes,
} from "./ImageQueue"

const imageQueue = new ImageQueue();

export default (images) => {
    return promiseSettle(createJobs(images).map((job) => imageQueue.push(job)))
}

/*
    IMAGE
*/

// Vehicles detection
const detectVehicles = (images) => {
    return images.map((image) => ({
        // Thumbnail
        type: ImageQueueJobTypes.VehiclesDetection,
        params: {
            image,
        },
        options: {
            // priority: 1,
        }
    }))
}


// Filter image media and create jobs
const createJobs = (images) => {
    return [
        // By priority
        ...detectVehicles(images),
    ]
}
