
import del from "del"
import pathLib from "path"

/*
    API
*/
export const buildAPIUrl = (model="", id="") => {
    return __API_URL__ + pathLib.join(model, "" + id);
}

/*
    File
*/

export const parseFiles = (rawFiles) => {
    return rawFiles.map(({
        name,
        path,
    }) => ({
        name,
        path,
        uuid: pathLib.basename(path),
        cameraId: pathLib.basename(name, ".jpg").split("_")[0],
        timestamp: pathLib.basename(name, ".jpg").split("_")[1],
    }))
}

export const parseDatasetFiles = (rawFiles) => {
    return rawFiles.map(({
        name,
        path,
    }) => ({
        name:pathLib.basename(name),
        path,
        uuid: pathLib.basename(path),
        cameraId: name.split("/")[0],
    }))
}


/*
    Middleware
*/

// Clean the media folder every request
// (Use in dev only)
export const cleanMediaFolder = (enable=true) => {
    return (res, req, next) => {
        // Make sure we are in dev
        if(__DEV__ && enable) {

            // Delete content of those folders
            del([
                `${__MEDIA_TMP_DIR__}/*`,
                `${__IMAGE_DIR__}/*`,
                `${__DATASETS_DIR__}/*`,
            ], {force: true})
            .then((paths) => {
                console.log("--->".green + ` ${paths.length} files/folders were delete `.cyan + "<---".green)
                next();
            })
            .catch((err) => {
                console.log(err);
                next();
            })

        } else {
            next();
        }
    }
}
