"use strict";

/*
    Express config
*/


import express from "express"
import morgan from "morgan"
import bodyParser from "body-parser"
import errorHandler from "errorhandler"
import pathLib from "path"
import fs from "fs"

import routes from "../upload"

export default function(config) {
    const app = express();
    const env = app.get("env");

    if(__DEV__) {
        app.use((req, res, next) => {
            console.log("REQEST".green, Date.now())
            next();
        })
        // Logger
        app.use(morgan("dev"));
    }

    // BodyParser
    app.use(bodyParser.urlencoded({ extended: false }));
    // app.use(bodyParser.json({limit: "50mb"}));
    // app.use(bodyParser.text({limit: "50mb", type: "text/plain"}));
    // app.use(bodyParser.raw({limit: "50mb", type: "text/plain"}));

    // Routes
    app.use("/upload", routes);

    // Error handler - must be last
    if(__DEV__) {
        app.use(errorHandler());
    }

    return Promise.resolve(app);
}
