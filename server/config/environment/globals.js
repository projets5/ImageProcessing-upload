/*
    Define your globals server side here

    We use the __...__ naming schem
*/

import path from "path";

process.env.NODE_ENV = process.env.NODE_ENV || "development";
global.__ENV__ = process.env.NODE_ENV;

global.__DEV__ = __ENV__ === "development";
global.__TEST__ = __ENV__ === "test"; // Not really use
global.__PROD__ = __ENV__ === "production";

// Paths
global.__ROOT_DIR__ = path.join(__dirname, "..", "..", "..");

global.__SERVER_DIR__ = path.join(__ROOT_DIR__, "server");
global.__CONFIG_DIR__ = path.join(__SERVER_DIR__, "config");
global.__ENV_DIR__ = path.join(__SERVER_DIR__, "environment");

global.__MEDIA_DIR__ = path.join(__ROOT_DIR__, "..", "media");
global.__CAMERAS_DIR__ = path.join(__MEDIA_DIR__, "cameras");
global.__DATASETS_DIR__ = path.join(__MEDIA_DIR__, "Datasets");
global.__IMAGE_DIR__ = path.join(__MEDIA_DIR__, "image");
global.__MEDIA_TMP_DIR__ = path.join(__MEDIA_DIR__, "tmp");

global.__NODE_MODULES_DIR__ = path.join(__ROOT_DIR__, "node_modules");

// URLS
global.__API_URL__ = `http://${process.env.NGINX_HOST}:80/api/v0/`;
global.__REDIS_URL__ = `redis://${process.env.REDIS_HOST}:${process.env.REDIS_PORT || 6379}`;

global.__CAMERA_PLACEHOLDER__ = path.join(__SERVER_DIR__, "upload", "imgs", "camera_placeholder.png");
