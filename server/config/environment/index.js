'use strict';

/*
    Default config of this server

    You can define config base on the NODE_ENV variable
        development.js
        staging.js
        production.js

*/
var all = {
    // Server port
    port: process.env.PORT || 3000,

    // Server IP
    ip: process.env.IP || "0.0.0.0",
};

// Extend and export the config base on NODE_ENV
module.exports = {
    ...all,

    // require the right config
    ...require(`./${process.env.NODE_ENV}.js`) || {}
}
